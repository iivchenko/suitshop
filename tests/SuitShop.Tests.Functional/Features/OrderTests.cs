﻿using NUnit.Framework;
using RestEase;
using SuitShop.Tests.Functional.Infrastructure.DataBase;
using SuitShop.Tests.Functional.Infrastructure.Services;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace SuitShop.Tests.Functional.Features
{
    [TestFixture]
    public sealed class OrderTests
    {
        private ISuitShopApiClient _client;
        private OrderRepository _orderRepository;

        [OneTimeSetUp]
        public void Initialize()
        {
            _client = RestClient.For<ISuitShopApiClient>("http://localhost:7780/api/");
        }

        [SetUp]
        public void Setup()
        {
            _orderRepository = new OrderRepository("Server=localhost,5433;Database=suitshop;User Id=sa;Password=Pass@word");
        }

        [TearDown]
        public void TearDown()
        {
            _orderRepository.Dispose();
        }

        [Test]
        public async Task GetOrders_OrdersExist_ReturnOrdersWithAlterations()
        {
            // Arrange
            var orderEntity1 = _orderRepository.CreateOrder("Test1", OrderStatus.Created, 1, 0, 0, 0);
            var orderEntity2 = _orderRepository.CreateOrder("Test2", OrderStatus.Paid, 1, 2, 0, 0);
            var orderEntity3 = _orderRepository.CreateOrder("Test3", OrderStatus.InProcess, 1, 2, 3, 0);
            var orderEntity4 = _orderRepository.CreateOrder("Test4", OrderStatus.Done, 1, 2, 3, 4);

            // Act
            var orders = await _client.GetOrders(0);

            // 
            Assert.Multiple(() =>
            {
                Assert.That(orders, Contains.Item(orderEntity1));
                Assert.That(orders, Contains.Item(orderEntity2));
                Assert.That(orders, Contains.Item(orderEntity3));
                Assert.That(orders, Contains.Item(orderEntity4));
            });
        }

        [Test]
        public void CreateOrder_MissingDescription_Fail()
        {
            // Assert
            var model = new OrderCreateModel
            {
                Description = string.Empty,
                Alterations = new List<CreateAlterationModel>
                {
                    new CreateAlterationModel
                    {
                        Amount = 1,
                        Side = Infrastructure.Services.AlterationSide.Left,
                        Type = Infrastructure.Services.AlterationType.Sleeve
                    },

                    new CreateAlterationModel
                    {
                        Amount = 2,
                        Side = Infrastructure.Services.AlterationSide.Right,
                        Type = Infrastructure.Services.AlterationType.Sleeve
                    },
                    new CreateAlterationModel
                    {
                        Amount = 3,
                        Side = Infrastructure.Services.AlterationSide.Left,
                        Type = Infrastructure.Services.AlterationType.Trouser
                    },
                    new CreateAlterationModel
                    {
                        Amount = 4,
                        Side = Infrastructure.Services.AlterationSide.Right,
                        Type = Infrastructure.Services.AlterationType.Trouser
                    }
                }
            };

            // Act + Assert
            Assert
               .That(
                   async () => await _client.CreateOrder(model),
                   Throws.InstanceOf<ApiException>().With
                       .Property("StatusCode").EqualTo(HttpStatusCode.BadRequest)
                       .And
                       .Property("Content").Match("'Description' must not be empty."));
        }

        [Test]
        public void CreateOrder_DuplicatedAlteration_Fail()
        {
            // Assert
            var model = new OrderCreateModel
            {
                Description = "Hello guys",
                Alterations = new List<CreateAlterationModel>
                {
                    new CreateAlterationModel
                    {
                        Amount = 1,
                        Side = Infrastructure.Services.AlterationSide.Left,
                        Type = Infrastructure.Services.AlterationType.Sleeve
                    },

                    new CreateAlterationModel
                    {
                        Amount = 1,
                        Side = Infrastructure.Services.AlterationSide.Left,
                        Type = Infrastructure.Services.AlterationType.Sleeve
                    }                   
                }
            };

            // Act + Assert
            Assert
               .That(
                   async () => await _client.CreateOrder(model),
                   Throws.InstanceOf<ApiException>().With
                       .Property("StatusCode").EqualTo(HttpStatusCode.BadRequest)
                       .And
                       .Property("Content").Match("Aleration with Type Sleeve and Side Left allready exists!"));
        }

        [Test]
        public async Task CreateOrder_TestFlow()
        {
            // Assert
            var model = new OrderCreateModel
            {
                Description = "Hello guys",
                Alterations = new List<CreateAlterationModel>
                {
                    new CreateAlterationModel
                    {
                        Amount = 1,
                        Side = Infrastructure.Services.AlterationSide.Left,
                        Type = Infrastructure.Services.AlterationType.Sleeve
                    },

                    new CreateAlterationModel
                    {
                        Amount = 2,
                        Side = Infrastructure.Services.AlterationSide.Right,
                        Type = Infrastructure.Services.AlterationType.Sleeve
                    },
                    new CreateAlterationModel
                    {
                        Amount = 3,
                        Side = Infrastructure.Services.AlterationSide.Left,
                        Type = Infrastructure.Services.AlterationType.Trouser
                    },
                    new CreateAlterationModel
                    {
                        Amount = 4,
                        Side = Infrastructure.Services.AlterationSide.Right,
                        Type = Infrastructure.Services.AlterationType.Trouser
                    }
                }
            };

            // Act
            var order = await _client.CreateOrder(model);

            // Assert
            Assert.That(order, Is.EqualTo(model));
        }
    }
}
