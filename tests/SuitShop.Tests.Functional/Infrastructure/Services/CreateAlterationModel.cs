﻿namespace SuitShop.Tests.Functional.Infrastructure.Services
{
    public sealed class CreateAlterationModel
    {
        public AlterationType Type { get; set; }

        public AlterationSide Side { get; set; }

        public int Amount { get; set; }
    }
}
