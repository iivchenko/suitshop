﻿namespace SuitShop.Tests.Functional.Infrastructure.Services
{
    public class AlterationModel
    {
        public string Type { get; set; }

        public string Side { get; set; }

        public int Amount { get; set; }

        public override bool Equals(object obj)
        {
            switch (obj)
            {
                case null:
                    return false;

                case AlterationModel alteration:
                    return
                        Type == alteration.Type.ToString()
                        && Side == alteration.Side.ToString()
                        && Amount == alteration.Amount;

                case CreateAlterationModel alteration:
                    return
                        Type == alteration.Type.ToString()
                        && Side == alteration.Side.ToString()
                        && Amount == alteration.Amount;

                default:
                    return false;
            }
        }
    }
}