﻿using System;
using System.Collections.Generic;

namespace SuitShop.Tests.Functional.Infrastructure.Services
{
    public class OrderModel
    {
        public Guid Id { get; set; }

        public string Description { get; set; }

        public string Status { get; set; }

        public IEnumerable<AlterationModel> Alterations { get; set; }

        public override bool Equals(object obj)
        {
            switch (obj)
            {
                case null:
                    return false;

                case OrderModel order:
                    return Id == order.Id;

                case OrderCreateModel order:
                    return                       
                        Description == order.Description                        
                        && CompareAlterations(order.Alterations);

                default:
                    return false;
            }
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id);
        }

        private bool CompareAlterations(IEnumerable<CreateAlterationModel> alterations)
        {
            foreach (var entityAlteration in Alterations)
            {
                var equal = false;

                foreach (var alteration in alterations)
                {
                    if (entityAlteration.Equals(alteration))
                    {
                        equal = true;
                        break;
                    }
                }

                if (!equal)
                {
                    return false;
                }
            }

            return true;
        }
    }
}