﻿namespace SuitShop.Tests.Functional.Infrastructure.Services
{
    public enum AlterationType
    {
        Sleeve = 1,
        Trouser = 2
    }
}
