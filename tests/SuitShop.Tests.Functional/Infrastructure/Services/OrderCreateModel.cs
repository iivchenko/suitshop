﻿using System.Collections.Generic;

namespace SuitShop.Tests.Functional.Infrastructure.Services
{
    public sealed class OrderCreateModel
    {
        public string Description { get; set; }

        public IEnumerable<CreateAlterationModel> Alterations { get; set; }

        public override bool Equals(object obj)
        {
            switch (obj)
            {
                case null:
                    return false;

                case OrderModel order:
                    return order.Equals(this);

                default:
                    return false;
            }
        }
    }
}
