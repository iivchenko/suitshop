﻿using RestEase;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SuitShop.Tests.Functional.Infrastructure.Services
{
    public interface ISuitShopApiClient
    {
        [Get("orders")]
        Task<IEnumerable<OrderModel>> GetOrders([Query] int filter);

        [Post("orders")]
        Task<OrderModel> CreateOrder([Body]OrderCreateModel order);
    }
}
