﻿namespace SuitShop.Tests.Functional.Infrastructure.Services
{
    public enum AlterationSide
    {
        Left = 1,
        Right = 2
    }
}
