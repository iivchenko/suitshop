﻿namespace SuitShop.Tests.Functional.Infrastructure.DataBase
{
    public enum AlterationSide
    {
        Left = 1,
        Right = 2
    }
}
