﻿using SuitShop.Tests.Functional.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SuitShop.Tests.Functional.Infrastructure.DataBase
{
    public sealed class OrderEntity
    {
        public OrderEntity()
        {
            Alterations = new Collection<AlterationEntity>();
        }

        public Guid Id { get; set; }

        public string Description { get; private set; }

        public OrderStatus StatusId { get; private set; }

        public Collection<AlterationEntity> Alterations { get; set; }

        public override bool Equals(object obj)
        {
            switch (obj)
            {
                case null:
                    return false;

                case OrderEntity order:
                    return Id == order.Id;

                case OrderModel order:
                    return
                        Id == order.Id
                        && Description == order.Description
                        && StatusId == (OrderStatus)Enum.Parse(typeof(OrderStatus), order.Status)
                        && CompareAlterations(order.Alterations);

                default:
                    return false;
            }
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id);
        }

        private bool CompareAlterations(IEnumerable<AlterationModel> alterations)
        {
            foreach(var entityAlteration in Alterations)
            {
                var equal = false;

                foreach(var alteration in alterations)
                {
                    if (entityAlteration.Equals(alteration))
                    {
                        equal = true;
                        break;
                    }
                }

                if (!equal)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
