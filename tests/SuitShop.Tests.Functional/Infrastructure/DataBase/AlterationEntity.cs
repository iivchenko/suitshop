﻿using SuitShop.Tests.Functional.Infrastructure.Services;
using System;

namespace SuitShop.Tests.Functional.Infrastructure.DataBase
{
    public sealed class AlterationEntity
    {
        public Guid Id { get; set; }

        public AlterationType TypeId { get; private set; }

        public AlterationSide SideId { get; private set; }

        public int Amount { get; private set; }

        public override bool Equals(object obj)
        {
            switch (obj)
            {
                case null:
                    return false;

                case AlterationEntity alteration:
                    return Id == alteration.Id;

                case AlterationModel alteration:
                    return
                        TypeId == (AlterationType)Enum.Parse(typeof(AlterationType), alteration.Type)
                        && SideId == (AlterationSide)Enum.Parse(typeof(AlterationSide), alteration.Side)
                        && Amount == alteration.Amount;

                default:
                    return false;
            }
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id);
        }
    }
}
