﻿namespace SuitShop.Tests.Functional.Infrastructure.DataBase
{
    public enum OrderStatus
    {
        Created = 1,
        Paid = 2,
        InProcess = 3,
        Done = 4
    }
}
