﻿namespace SuitShop.Tests.Functional.Infrastructure.DataBase
{
    public enum AlterationType
    {
        Sleeve = 1, 
        Trouser = 2
    }
}
