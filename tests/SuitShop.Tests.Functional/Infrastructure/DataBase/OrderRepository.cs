﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SuitShop.Tests.Functional.Infrastructure.DataBase
{
    public sealed class OrderRepository : IDisposable
    {
        private readonly SqlConnection _sqlConnection;
        private readonly List<Guid> _orders;

        public OrderRepository(string connectionString)
        {
            _sqlConnection = new SqlConnection(connectionString);
            _orders = new List<Guid>();
        }

        public OrderEntity CreateOrder(
            string description,
            OrderStatus status, 
            int leftSleeve,
            int rightSleeve,
            int leftTrouser,
            int rightTrouser)
        {
            var sqlOrder = "INSERT INTO [dbo].[orders](Id, Description, StatusId) OUTPUT INSERTED.* VALUES(@Id, @Description, @StatusId);";           

            var order =
                _sqlConnection
                    .QuerySingle<OrderEntity>
                    (
                        sqlOrder,
                        new
                        {
                            Id = Guid.NewGuid(),
                            Description = description,
                            StatusId = (int)status
                        }
                    );

            if (leftSleeve > 0)
            {
                order.Alterations.Add(CreateAlteratioin(_sqlConnection, AlterationType.Sleeve, AlterationSide.Left, leftSleeve, order.Id));
            }

            if (rightSleeve > 0)
            {
                order.Alterations.Add(CreateAlteratioin(_sqlConnection, AlterationType.Sleeve, AlterationSide.Right, rightSleeve, order.Id));
            }

            if (leftTrouser > 0)
            {
                order.Alterations.Add(CreateAlteratioin(_sqlConnection, AlterationType.Trouser, AlterationSide.Left, leftTrouser, order.Id));
            }

            if (rightTrouser > 0)
            {
                order.Alterations.Add(CreateAlteratioin(_sqlConnection, AlterationType.Trouser, AlterationSide.Right, rightTrouser, order.Id));
            }

            _orders.Add(order.Id);

            return order;
        }

        private static AlterationEntity CreateAlteratioin(SqlConnection connection, AlterationType typeId, AlterationSide sideId, int amount, Guid orderId)
        {
            var sqlAlteration = "INSERT INTO [dbo].[alterations](Id, TypeId, SideId, Amount, OrderId) OUTPUT INSERTED.* VALUES(@Id, @TypeId, @SideId, @Amount, @OrderId);";

            return
                connection
                    .QuerySingle<AlterationEntity>
                    (
                        sqlAlteration,
                        new
                        {
                            Id = Guid.NewGuid(),
                            TypeId = typeId,
                            SideId = sideId,
                            Amount = amount,
                            OrderId = orderId
                        }
                    );
        }

        public void Dispose()
        {
            foreach (var id in _orders)
            {
                var sql = "DELETE [dbo].[alterations] WHERE OrderId = @Id; DELETE [dbo].[orders] WHERE Id = @Id";

                _sqlConnection.Execute(sql, new { Id = id });
            }

            _sqlConnection.Dispose();
        }
    }
}
