﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SuitShop.Core.Application.Queries.Sql.Common;
using SuitShop.Infrastructure.Persistence;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Reflection;
using AutoMapper;
using MediatR;
using SuitShop.Core.Application.Commands;
using SuitShop.Core.Domain.Orders;
using SuitShop.Core.Domain.Common;
using SuitShop.Infrastructure.Persistence.Orders;
using SuitShop.Api.Messaging;
using SuitShop.Api.Orders;
using SuitShop.Api.Messaging.Orders;
using SuitShop.Core.Application.Commands.Services;
using SuitShop.Infrastructure.Services;
using EasyNetQ;
using ISqlConnectionFactory = SuitShop.Core.Application.Queries.Sql.Common.IConnectionFactory;
using Microsoft.Azure.ServiceBus;
using SuitShop.Api.Infrastructure;
using FluentValidation.AspNetCore;

namespace SuitShop.Api
{
    public sealed class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // Common
            services.AddSingleton<ISqlConnectionFactory>(new ConnectionFactory(Configuration["ConnectionString"]));
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            // Order
            services.AddScoped<IOrderFactory, OrderFactory>();
            services.AddScoped<IOrderRepository, OrderRepository>();

            // Alteratoin
            services.AddScoped<IAlteratioinFactory, AlteratioinFactory>();

            services.AddHostedService<AzureServiceBusHostedService>();
            services.AddScoped<IMessageService>(x => new AzureMessageService(new QueueClient(Configuration["AzureBusConnectionString"], "AlterationDone")));
            //services.AddHostedService<RabbitMqHostedService>(); // TODO: I need Rabbit as I can't test it with Azure.
            //services.AddScoped<IMessageService>(x => new RabbitMqMessageService(RabbitHutch.CreateBus(Configuration["RabbitMqConnectionString"]))); // TODO: I need Rabbit as I can't test it with Azure.

            services
                 .AddDbContext<SuitShopContext>(
                     options =>
                     {
                         options
                             .UseSqlServer(
                                 Configuration["ConnectionString"],
                                 sqlOptions => sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name));
                     });

            services
               .AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services
                .AddMediatR(
                    typeof(ApiMapping).GetTypeInfo().Assembly,
                    typeof(CommandsMapping).GetTypeInfo().Assembly,
                    typeof(ISqlConnectionFactory).GetTypeInfo().Assembly);

            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(LoggingBehavior<,>));
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));

            services
                .AddMvc()
                .AddFluentValidation(x => x.RegisterValidatorsFromAssemblies(new[] { typeof(CommandsMapping).GetTypeInfo().Assembly }))
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services
                .AddSwaggerGen(c =>
                {
                    c.DescribeAllEnumsAsStrings();

                    c.SwaggerDoc("v1", new Info
                    {
                        Version = "v1",
                        Title = "Suit Shop Api",
                        Contact = new Contact
                        {
                            Name = "Suit Shop",
                            Email = "iivchenko@live.com",
                            Url = "https://iivchenko@bitbucket.org/iivchenko/suitshop"
                        }
                    });
                });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Better approach is to use EF migrations. As it is not a requirenment in the Assesment will live simple solution for now.
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<SuitShopContext>();
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                context.AlterationSides.Add(AlterationSide.Left);
                context.AlterationSides.Add(AlterationSide.Right);

                context.AlterationTypes.Add(AlterationType.Sleeve);
                context.AlterationTypes.Add(AlterationType.Trouser);

                context.OrderStatuses.Add(OrderStatus.Created);
                context.OrderStatuses.Add(OrderStatus.Paid);
                context.OrderStatuses.Add(OrderStatus.InProcess);
                context.OrderStatuses.Add(OrderStatus.Done);

                context.SaveChanges();
            }

            app.UseMiddleware<ExceptionHandlingMiddleware>();

            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Suit Shop Api V1");
            });
        }
    }
}
