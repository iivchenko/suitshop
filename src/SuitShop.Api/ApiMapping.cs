﻿using AutoMapper;
using SuitShop.Api.Orders.Models;
using SuitShop.Core.Application.Commands.CreateOrder;
using SuitShop.Core.Application.Queries.GetOrders;

namespace SuitShop.Api
{
    public sealed class ApiMapping : Profile
    {
        public ApiMapping()
        {
            // Queries
            CreateMap<GetOrdersQueryResult, OrderModel>();
            CreateMap<GetOrdersQueryAlteration, OrderAlterationModel>();
            CreateMap<OrderStatusFilter, GetOrdersQueryFilter>();

            // Commands
            CreateMap<OrderCreateModel, CreateOrderCommand>();
            CreateMap<OrderCreateAlterationModel, CreateOrderCommandAlteration>();
            CreateMap<CreateOrderCommandResult, OrderModel>();
            CreateMap<CreateOrderCommandAlteration, OrderAlterationModel>();
            CreateMap<OrderAlterationType, CreateOrderCommandAlterationType>();
            CreateMap<OrderAlterationSide, CreateOrderCommandAlterationSide>();
        }
    }
}
