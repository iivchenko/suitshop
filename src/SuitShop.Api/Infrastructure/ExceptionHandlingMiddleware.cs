﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using SuitShop.Core.Domain.Common;
using System.Threading.Tasks;

namespace SuitShop.Api.Infrastructure
{
    public sealed class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch(DomainException e)
            {
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
                context.Response.ContentType = "application/json";
                var error = new
                {
                    ErrorCode = StatusCodes.Status400BadRequest,
                    Message = e.Message
                };

                await context.Response.WriteAsync(JsonConvert.SerializeObject(error));
            }
            catch (ValidationException e)
            {
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
                context.Response.ContentType = "application/json";
                var error = new
                {
                    ErrorCode = StatusCodes.Status400BadRequest,
                    Message = e.Message
                };

                await context.Response.WriteAsync(JsonConvert.SerializeObject(error));
            }
            catch
            {
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;                
            }
        }
    }
}
