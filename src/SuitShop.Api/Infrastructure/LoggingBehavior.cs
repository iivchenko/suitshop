﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;

namespace SuitShop.Api.Infrastructure
{
    public sealed class LoggingBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly ILogger<LoggingBehavior<TRequest, TResponse>> _logger;

        public LoggingBehavior(ILogger<LoggingBehavior<TRequest, TResponse>> logger)
        {
            _logger = logger;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            try
            {
                _logger.LogInformation($"Start executing request {typeof(TRequest)}!");

                var result = await next();

                _logger.LogInformation($"Finish executing request {typeof(TRequest)}!");

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Fail executing request {typeof(TRequest)}!");
                throw;
            }           
        }
    }
}
