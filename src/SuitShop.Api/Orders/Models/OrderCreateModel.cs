﻿using System.Collections.Generic;

namespace SuitShop.Api.Orders.Models
{
    public sealed class OrderCreateModel
    {
        public string Description { get; set; }

        public IEnumerable<OrderCreateAlterationModel> Alterations { get; set; }
    }
}
