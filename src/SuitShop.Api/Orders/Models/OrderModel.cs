﻿using System;
using System.Collections.Generic;

namespace SuitShop.Api.Orders.Models
{
    public sealed class OrderModel
    {
        public Guid Id { get; set; }

        public string Description { get; set; }

        public string Status { get; set; }

        public IEnumerable<OrderAlterationModel> Alterations { get; set; }
    }
}
