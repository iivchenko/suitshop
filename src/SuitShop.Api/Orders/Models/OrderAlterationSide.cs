﻿namespace SuitShop.Api.Orders.Models
{
    public enum OrderAlterationSide
    {
        Left = 1,
        Right = 2
    }
}
