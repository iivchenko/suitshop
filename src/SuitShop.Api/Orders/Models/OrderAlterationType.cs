﻿namespace SuitShop.Api.Orders.Models
{
    public enum OrderAlterationType
    {
        Sleeve = 1,
        Trouser = 2
    }
}
