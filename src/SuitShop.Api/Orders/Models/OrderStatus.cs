﻿namespace SuitShop.Api.Orders.Models
{
    public enum OrderStatus
    {
        InProcess,
        Done
    }
}
