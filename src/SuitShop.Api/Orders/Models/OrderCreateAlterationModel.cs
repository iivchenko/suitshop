﻿namespace SuitShop.Api.Orders.Models
{
    public sealed class OrderCreateAlterationModel
    {
        public OrderAlterationType Type { get; set; }

        public OrderAlterationSide Side { get; set; }

        public int Amount { get; set; }
    }
}
