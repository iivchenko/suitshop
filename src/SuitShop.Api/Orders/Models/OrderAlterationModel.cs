﻿namespace SuitShop.Api.Orders.Models
{
    public sealed class OrderAlterationModel
    {
        public string Type { get; set; }

        public string Side { get; set; }

        public int Amount { get; set; }
    }
}
