﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using SuitShop.Api.Orders.Models;
using SuitShop.Core.Application.Commands.CreateOrder;
using SuitShop.Core.Application.Commands.FinishOrderProcessing;
using SuitShop.Core.Application.Commands.PayOrder;
using SuitShop.Core.Application.Commands.StartOrderProcessing;
using SuitShop.Core.Application.Queries.GetOrders;

namespace SuitShop.Api.Orders
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public OrdersController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<OrderModel>> GetOrders(OrderStatusFilter filter)
        {
            var orders = await _mediator.Send(new GetOrdersQuery { Filter = _mapper.Map<GetOrdersQueryFilter>(filter) });

            return _mapper.Map<IEnumerable<OrderModel>>(orders);
        }

        [HttpPost]
        public async Task<OrderModel> CreateOrder([FromBody]OrderCreateModel orderInfo)
        {
            var order = await _mediator.Send(_mapper.Map<CreateOrderCommand>(orderInfo));

            return _mapper.Map<OrderModel>(order);
        }

        [HttpPut("{id}")]
        public async Task UpdateOrderStatus(Guid id, OrderStatus status)
        {
            switch (status)
            {
                case OrderStatus.InProcess:
                    var inProcessCommand = new StartOrderProcessingCommand { OrderId = id };
                    await _mediator.Send(inProcessCommand);
                    break;

                case OrderStatus.Done:
                    var finishCommand = new FinishOrderProcessingCommand { OrderId = id };
                    await _mediator.Send(finishCommand);
                    break;

                default:
                    throw new Exception();
            }
        }
    }
}
