﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MediatR;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Hosting;
using SuitShop.Core.Application.Commands.PayOrder;
using Microsoft.Extensions.DependencyInjection;

namespace SuitShop.Api.Messaging.Orders
{
    public sealed class AzureServiceBusHostedService : IHostedService
    {
        private readonly IConfiguration _configuration;
        private readonly IServiceProvider _services;

        private QueueClient _orderPaidQueue;

        public AzureServiceBusHostedService(IConfiguration configuration, IServiceProvider services)
        {
            _configuration = configuration;
            _services = services;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            var messageHandlerOptions = new MessageHandlerOptions(HandleException)
            {
                MaxConcurrentCalls = 1,
                AutoComplete = false
            };

            _orderPaidQueue = new QueueClient(_configuration["AzureBusConnectionString"], "OrderPaidTopic");
            _orderPaidQueue.RegisterMessageHandler(HandleOrderPaidMessage, messageHandlerOptions);

            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await _orderPaidQueue.CloseAsync();
        }

        private Task HandleException(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {            
            return Task.CompletedTask;
        }

        private async Task HandleOrderPaidMessage(Message message, CancellationToken token)
        {
            var orderId = Guid.Parse(Encoding.UTF8.GetString(message.Body));

            using (var scope = _services.CreateScope())
            {
                var mediator = scope.ServiceProvider.GetRequiredService<IMediator>();

                await mediator.Send(new PayOrderCommand { OrderId = orderId });
            }

            await _orderPaidQueue.CompleteAsync(message.SystemProperties.LockToken);
        }
    }
}
