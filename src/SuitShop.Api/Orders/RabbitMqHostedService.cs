﻿using System;
using System.Threading;
using System.Threading.Tasks;
using EasyNetQ;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SuitShop.Core.Application.Commands.PayOrder;

namespace SuitShop.Api.Orders
{
    public sealed class RabbitMqHostedService : IHostedService
    {
        private readonly IServiceProvider _services;
        private readonly IConfiguration _configuration;
        private IBus _bus;

        public RabbitMqHostedService(IConfiguration configuration, IServiceProvider services)
        {
            _configuration = configuration;
            _services = services;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _bus = RabbitHutch.CreateBus(_configuration["RabbitMqConnectionString"]);

            _bus.Subscribe<OrderPaidMessage>("SuitShop", HandleTextMessage);

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _bus.Dispose();

            return Task.CompletedTask;
        }

        private void HandleTextMessage(OrderPaidMessage message)
        {
            using (var scope = _services.CreateScope())
            {
                var mediator = scope.ServiceProvider.GetRequiredService<IMediator>();

                Task.WaitAll(mediator.Send(new PayOrderCommand { OrderId = message.OrderId }));
            }
        }
    }

    public sealed class OrderPaidMessage
    {
        public Guid OrderId { get; set; }
    }
}
