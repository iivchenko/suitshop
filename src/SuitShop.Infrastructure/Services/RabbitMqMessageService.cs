﻿using System;
using System.Threading.Tasks;
using EasyNetQ;
using SuitShop.Core.Application.Commands.Services;

namespace SuitShop.Infrastructure.Services
{
    public sealed class RabbitMqMessageService : IMessageService, IDisposable
    {
        private readonly IBus _bus;

        public RabbitMqMessageService(IBus bus)
        {
            _bus = bus;
        }

        public async Task Send<TMessage>(TMessage message) where TMessage : class
        {
            await _bus.PublishAsync(message);
        }

        public void Dispose()
        {
            _bus.Dispose();
        }
    }
}
