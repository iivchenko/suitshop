﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using SuitShop.Core.Application.Commands.Services;

namespace SuitShop.Infrastructure.Services
{
    public sealed class AzureMessageService : IMessageService, IDisposable
    {
        private readonly QueueClient _queueClient;

        public AzureMessageService(QueueClient queueClient)
        {
            _queueClient = queueClient;
        }        

        public async Task Send<TMessage>(TMessage message) where TMessage : class
        {
            await _queueClient.SendAsync(Convert(message));
        }

        private static Message Convert<TMessage>(TMessage message)
        {
            return new Message(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message)));
        }

        public async void Dispose()
        {
            await _queueClient.CloseAsync();
        }
    }
}