﻿using MediatR;
using System;

namespace SuitShop.Core.Application.Commands.StartOrderProcessing
{
    public sealed class StartOrderProcessingCommand : IRequest
    {
        public Guid OrderId { get; set; }
    }
}
