﻿using FluentValidation;

namespace SuitShop.Core.Application.Commands.StartOrderProcessing
{
    public sealed class StartOrderProcessingCommandValidator : AbstractValidator<StartOrderProcessingCommand>
    {
        public StartOrderProcessingCommandValidator()
        {
            RuleFor(x => x.OrderId).NotEmpty();
        }
    }
}
