﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SuitShop.Core.Domain.Common;
using SuitShop.Core.Domain.Orders;

namespace SuitShop.Core.Application.Commands.StartOrderProcessing
{
    public sealed class StartOrderProcessingCommandHandler : IRequestHandler<StartOrderProcessingCommand>
    {
        private IOrderRepository _orderRepository;
        private IUnitOfWork _unitOfWork;

        public StartOrderProcessingCommandHandler(
            IOrderRepository customerRepository,
            IUnitOfWork unitOfWork)
        {
            _orderRepository = customerRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(StartOrderProcessingCommand request, CancellationToken cancellationToken)
        {
            var order = await _orderRepository.GetOrder(request.OrderId);

            order.TakeToWork();

            await _orderRepository.UpdateOrder(order);
            await _unitOfWork.Commit();

            return Unit.Value;
        }
    }
}
