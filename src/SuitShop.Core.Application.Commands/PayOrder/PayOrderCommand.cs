﻿using MediatR;
using System;

namespace SuitShop.Core.Application.Commands.PayOrder
{
    public sealed class PayOrderCommand : IRequest
    {
        public Guid OrderId { get; set; }
    }
}
