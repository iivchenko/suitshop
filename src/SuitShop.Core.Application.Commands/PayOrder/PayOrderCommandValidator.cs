﻿using FluentValidation;

namespace SuitShop.Core.Application.Commands.PayOrder
{
    public sealed class PayOrderCommandValidator : AbstractValidator<PayOrderCommand>
    {
        public PayOrderCommandValidator()
        {
            RuleFor(x => x.OrderId).NotEmpty();
        }
    }
}
