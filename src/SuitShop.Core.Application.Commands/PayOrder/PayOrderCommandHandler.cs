﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SuitShop.Core.Domain.Common;
using SuitShop.Core.Domain.Orders;

namespace SuitShop.Core.Application.Commands.PayOrder
{
    public sealed class PayOrderCommandHandler : IRequestHandler<PayOrderCommand>
    {
        private IOrderRepository _orderRepository;
        private IUnitOfWork _unitOfWork;

        public PayOrderCommandHandler(
            IOrderRepository customerRepository,
            IUnitOfWork unitOfWork)
        {
            _orderRepository = customerRepository;
            _unitOfWork = unitOfWork;
        }
        public async Task<Unit> Handle(PayOrderCommand request, CancellationToken cancellationToken)
        {
            var order = await _orderRepository.GetOrder(request.OrderId);

            order.Pay();

            await _orderRepository.UpdateOrder(order);
            await _unitOfWork.Commit();

            return Unit.Value;
        }
    }
}
