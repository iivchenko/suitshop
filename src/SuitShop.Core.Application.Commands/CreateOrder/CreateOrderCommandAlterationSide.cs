﻿namespace SuitShop.Core.Application.Commands.CreateOrder
{
    public enum CreateOrderCommandAlterationSide
    {
        Left = 1,
        Right = 2
    }
}
