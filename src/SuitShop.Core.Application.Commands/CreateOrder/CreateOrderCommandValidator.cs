﻿using FluentValidation;

namespace SuitShop.Core.Application.Commands.CreateOrder
{
    public sealed class CreateOrderCommandValidator : AbstractValidator<CreateOrderCommand>
    {
        public CreateOrderCommandValidator()
        {
            RuleFor(x => x.Description).NotEmpty().MaximumLength(50);
            RuleFor(x => x.Alterations).NotEmpty().WithMessage("Provide at least one alteratioins!");
        }
    }
}
