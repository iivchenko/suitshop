﻿namespace SuitShop.Core.Application.Commands.CreateOrder
{
    public enum CreateOrderCommandAlterationType
    {
        Sleeve = 1,
        Trouser = 2
    }
}
