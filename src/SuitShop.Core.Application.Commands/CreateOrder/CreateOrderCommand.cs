﻿using MediatR;
using System.Collections.Generic;

namespace SuitShop.Core.Application.Commands.CreateOrder
{
    public sealed class CreateOrderCommand : IRequest<CreateOrderCommandResult>
    {
        public string Description { get; set; }

        public IEnumerable<CreateOrderCommandAlteration> Alterations { get; set; }
    }
}
