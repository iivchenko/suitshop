﻿using System;
using System.Collections.Generic;

namespace SuitShop.Core.Application.Commands.CreateOrder
{
    public sealed class CreateOrderCommandResult
    {
        public Guid Id { get; set; }

        public string Description { get; set; }

        public string Status { get; set; }

        public IEnumerable<CreateOrderCommandAlteration> Alterations { get; set; }
    }
}
