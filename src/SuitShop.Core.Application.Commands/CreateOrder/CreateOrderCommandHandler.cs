﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using SuitShop.Core.Domain.Common;
using SuitShop.Core.Domain.Orders;

namespace SuitShop.Core.Application.Commands.CreateOrder
{
    public sealed class CreateOrderCommandHandler : IRequestHandler<CreateOrderCommand, CreateOrderCommandResult>
    {
        private readonly IOrderFactory _orderFactory;
        private readonly IAlteratioinFactory _alteratioinFactory;
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CreateOrderCommandHandler(
            IOrderFactory orderFactory,
            IAlteratioinFactory alteratioinFactory,
            IOrderRepository orderRepository,
            IMapper mapper,
            IUnitOfWork unitOfWork)
        {
            _orderFactory = orderFactory;
            _alteratioinFactory = alteratioinFactory;
            _orderRepository = orderRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<CreateOrderCommandResult> Handle(CreateOrderCommand request, CancellationToken cancellationToken)
        {
            var order = _orderFactory.Create(request.Description);

            foreach(var alterationInfo in request.Alterations)
            {
                var type = AlterationType.GetById((int)alterationInfo.Type);
                var side = AlterationSide.GetById((int)alterationInfo.Side);
                var amount = alterationInfo.Amount;
                var alteration = _alteratioinFactory.Create(type, side, amount);

                order.AddAlterations(alteration);
            }

            await _orderRepository.CreateOrder(order);
            await _unitOfWork.Commit();

            return _mapper.Map<Order, CreateOrderCommandResult>(order);
        }
    }
}
