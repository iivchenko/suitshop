﻿namespace SuitShop.Core.Application.Commands.CreateOrder
{
    public sealed class CreateOrderCommandAlteration
    {
        public CreateOrderCommandAlterationType Type { get; set; }

        public CreateOrderCommandAlterationSide Side { get; set; }

        public int Amount { get; set; }
    }
}
