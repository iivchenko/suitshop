﻿using System.Threading.Tasks;

namespace SuitShop.Core.Application.Commands.Services
{
    public interface IMessageService
    {
        Task Send<TMessage>(TMessage message) where TMessage : class;
    }
}
