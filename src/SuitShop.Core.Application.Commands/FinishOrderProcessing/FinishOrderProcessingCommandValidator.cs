﻿using FluentValidation;

namespace SuitShop.Core.Application.Commands.FinishOrderProcessing
{
    public sealed class FinishOrderProcessingCommandValidator : AbstractValidator<FinishOrderProcessingCommand>
    {
        public FinishOrderProcessingCommandValidator()
        {
            RuleFor(x => x.OrderId).NotEmpty();
        }
    }
}
