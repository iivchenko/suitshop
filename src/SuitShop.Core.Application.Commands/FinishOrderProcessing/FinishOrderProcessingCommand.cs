﻿using MediatR;
using System;

namespace SuitShop.Core.Application.Commands.FinishOrderProcessing
{
    public sealed class FinishOrderProcessingCommand : IRequest
    {
        public Guid OrderId { get; set; }
    }
}
