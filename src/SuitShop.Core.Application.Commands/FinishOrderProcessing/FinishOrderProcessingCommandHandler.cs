﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SuitShop.Core.Application.Commands.Messages;
using SuitShop.Core.Application.Commands.Services;
using SuitShop.Core.Domain.Common;
using SuitShop.Core.Domain.Orders;

namespace SuitShop.Core.Application.Commands.FinishOrderProcessing
{
    public sealed class FinishOrderProcessingCommandHandler : IRequestHandler<FinishOrderProcessingCommand>
    {
        private IOrderRepository _orderRepository;
        private IMessageService _messageService;
        private IUnitOfWork _unitOfWork;

        public FinishOrderProcessingCommandHandler(
            IOrderRepository customerRepository,
            IMessageService messageService,
            IUnitOfWork unitOfWork)
        {
            _orderRepository = customerRepository;
            _messageService = messageService;
            _unitOfWork = unitOfWork;
        }
        public async Task<Unit> Handle(FinishOrderProcessingCommand request, CancellationToken cancellationToken)
        {
            var order = await _orderRepository.GetOrder(request.OrderId);

            order.Finish();

            await _orderRepository.UpdateOrder(order);
            await _unitOfWork.Commit();
            await _messageService.Send(new AlterationFinished { OrderId = order.Id });

            return Unit.Value;
        }
    }
}
