﻿using AutoMapper;
using SuitShop.Core.Application.Commands.CreateOrder;
using SuitShop.Core.Domain.Orders;

namespace SuitShop.Core.Application.Commands
{
    public sealed class CommandsMapping : Profile
    {
        public CommandsMapping()
        {
            CreateMap<Order, CreateOrderCommandResult>()
                .ForMember(
                    dest => dest.Status,
                    opt => opt.MapFrom(src => src.Status.Name));

            CreateMap<Alteration, CreateOrderCommandAlteration>();

            CreateMap<AlterationType, CreateOrderCommandAlterationType>()
                .ConvertUsing(x => (CreateOrderCommandAlterationType)x.Id);

            CreateMap<AlterationSide, CreateOrderCommandAlterationSide>()
                .ConvertUsing(x => (CreateOrderCommandAlterationSide)x.Id);
        }
    }
}
