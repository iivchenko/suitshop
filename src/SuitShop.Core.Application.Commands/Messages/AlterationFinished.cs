﻿using System;

namespace SuitShop.Core.Application.Commands.Messages
{
    public sealed class AlterationFinished
    {
        public Guid OrderId { get; set; }
    }
}
