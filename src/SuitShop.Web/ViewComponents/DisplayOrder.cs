﻿using Microsoft.AspNetCore.Mvc;
using SuitShop.Web.Models;
using System.Threading.Tasks;

namespace SuitShop.Web.ViewComponents
{
    [ViewComponent]
    public class DisplayOrder : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(OrderModel order)
        {            
            return View(order);
        }
    }
}
