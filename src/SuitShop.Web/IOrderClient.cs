﻿using RestEase;
using SuitShop.Web.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SuitShop.Web
{
    public interface IOrderClient
    {
        [Get("orders")]
        Task<IEnumerable<OrderModel>> GetOrders([Query]OrderStatusFilter filter);

        [Get("orders/{id}")]
        Task<IEnumerable<OrderModel>> GetOrder([Path] string id);

        [Post("orders")]
        Task<OrderModel> CreateOrder([Body]OrderModel order);

        [Put("orders/{id}")]
        Task UpdateOrderStatus([Path]string id, [Query]string status);
    }
}
