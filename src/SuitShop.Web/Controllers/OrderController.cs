﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SuitShop.Web.Models;

namespace SuitShop.Web.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderClient _orderClient;

        public OrderController(IOrderClient orderClient)
        {
            _orderClient = orderClient;
        }       

        public async Task<IActionResult> Index(OrderStatusFilter filter = OrderStatusFilter.All)
        {
            return View(await _orderClient.GetOrders(filter));
        }

        [HttpGet]
        public IActionResult NewOrder()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> NewOrder(NewOrderModel order)
        {
            var alterations = new List<OrderAlterationModel>();         

            if (order.LeftSleeve != 0)
            {
                alterations.Add(new OrderAlterationModel { Type = "Sleeve", Side = "Left", Amount = order.LeftSleeve });
            }

            if (order.RightSleeve != 0)
            {
                alterations.Add(new OrderAlterationModel { Type = "Sleeve", Side = "Right", Amount = order.RightSleeve });
            }

            if (order.LeftTrouser != 0)
            {
                alterations.Add(new OrderAlterationModel { Type = "Trouser", Side = "Left", Amount = order.LeftTrouser });
            }

            if (order.RightTrouser != 0)
            {
                alterations.Add(new OrderAlterationModel { Type = "Trouser", Side = "Right", Amount = order.RightTrouser });
            }

            var newOrder = new OrderModel
            {
                Description = order.Description,

                Alterations = alterations
            };

            await _orderClient.CreateOrder(newOrder);            
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> UpdateStatus(string id, [FromQuery] string status)
        {
            await _orderClient.UpdateOrderStatus(id, status);

            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
