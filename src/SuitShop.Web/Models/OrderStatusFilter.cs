﻿namespace SuitShop.Web.Models
{
    public enum OrderStatusFilter
    {
        All = 0,
        Created = 1,
        Paid = 2,
        InProcess = 3,
        Done = 4
    }
}
