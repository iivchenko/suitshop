﻿namespace SuitShop.Web.Models
{
    public sealed class NewOrderModel
    {
        public string Description { get; set; }

        public int LeftSleeve { get; set; }

        public int RightSleeve { get; set; }

        public int LeftTrouser { get; set; }

        public int RightTrouser { get; set; }
    }
}
