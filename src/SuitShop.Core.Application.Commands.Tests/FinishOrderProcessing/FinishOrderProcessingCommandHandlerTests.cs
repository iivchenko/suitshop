﻿using FakeItEasy;
using NUnit.Framework;
using SuitShop.Core.Application.Commands.FinishOrderProcessing;
using SuitShop.Core.Application.Commands.Messages;
using SuitShop.Core.Application.Commands.Services;
using SuitShop.Core.Domain.Common;
using SuitShop.Core.Domain.Orders;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SuitShop.Core.Application.Commands.Tests.FinishOrderProcessing
{
    [TestFixture]
    public sealed class FinishOrderProcessingCommandHandlerTests
    {
        private const string Description = "TheDescription";

        private IOrderRepository _orderRepository;
        private IMessageService _messageService;
        private IUnitOfWork _unitOfWork;

        private FinishOrderProcessingCommandHandler _handler;

        [SetUp]
        public void Setup()
        {          
            _orderRepository = A.Fake<IOrderRepository>();
            _messageService = A.Fake<IMessageService>();
            _unitOfWork = A.Fake<IUnitOfWork>();

            _handler = new FinishOrderProcessingCommandHandler(
                _orderRepository,
                _messageService,
                _unitOfWork);
        }

        [Test]
        public async Task Handle_TestFlow()
        {
            // Arrange
            var orderId = Guid.NewGuid();
            var command = new FinishOrderProcessingCommand
            {
               OrderId = orderId
            };

            var order = new Order(orderId, Description);

            order.Pay();
            order.TakeToWork();

            A
                .CallTo(() => _orderRepository.GetOrder(orderId))
                .Returns(order);

            // Act
            await _handler.Handle(command, CancellationToken.None);

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(order.Status, Is.EqualTo(OrderStatus.Done));

                A
                    .CallTo(() => _orderRepository.GetOrder(orderId))
                    .MustHaveHappened();

                A
                   .CallTo(() => _orderRepository.UpdateOrder(order))
                   .MustHaveHappened();

                A
                   .CallTo(() => _unitOfWork.Commit())
                   .MustHaveHappened();

                A
                    .CallTo(() => _messageService.Send<AlterationFinished>(A<AlterationFinished>.Ignored))
                    .MustHaveHappened();
            });
        }
    }
}
