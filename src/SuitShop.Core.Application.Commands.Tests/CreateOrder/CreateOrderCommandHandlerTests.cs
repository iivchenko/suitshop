﻿using AutoMapper;
using FakeItEasy;
using NUnit.Framework;
using SuitShop.Core.Application.Commands.CreateOrder;
using SuitShop.Core.Domain.Common;
using SuitShop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SuitShop.Core.Application.Commands.Tests.CreateOrder
{
    [TestFixture]
    public sealed class CreateOrderCommandHandlerTests
    {
        private const string Description = "TheDescription";

        private IOrderFactory _orderFactory;
        private IAlteratioinFactory _alterationFactory;
        private IOrderRepository _orderRepository;
        private IMapper _mapper;
        private IUnitOfWork _unitOfWork;

        private CreateOrderCommandHandler _handler;

        [SetUp]
        public void Setup()
        {
            _orderFactory = A.Fake<IOrderFactory>();
            _alterationFactory = A.Fake<IAlteratioinFactory>();
            _orderRepository = A.Fake<IOrderRepository>();
            _mapper = A.Fake<IMapper>();
            _unitOfWork = A.Fake<IUnitOfWork>();

            _handler = new CreateOrderCommandHandler(
                _orderFactory,
                _alterationFactory,
                _orderRepository,
                _mapper,
                _unitOfWork);
        }

        [Test]
        public async Task Handle_TestFlow()
        {
            // Arrange
            var commandAlteration1 = new CreateOrderCommandAlteration
            {
                Type = CreateOrderCommandAlterationType.Sleeve,
                Side = CreateOrderCommandAlterationSide.Left,
                Amount = 1
            };
            var commandAlteration2 = new CreateOrderCommandAlteration
            {
                Type = CreateOrderCommandAlterationType.Sleeve,
                Side = CreateOrderCommandAlterationSide.Right,
                Amount = 2
            };
            var commandAlteration3 = new CreateOrderCommandAlteration
            {
                Type = CreateOrderCommandAlterationType.Trouser,
                Side = CreateOrderCommandAlterationSide.Left,
                Amount = 3
            };
            var commandAlteration4 = new CreateOrderCommandAlteration
            {
                Type = CreateOrderCommandAlterationType.Trouser,
                Side = CreateOrderCommandAlterationSide.Right,
                Amount = 4
            };

            var command = new CreateOrderCommand
            {
                Description = Description, 
                Alterations = new List<CreateOrderCommandAlteration>
                {
                   commandAlteration1,
                   commandAlteration2,
                   commandAlteration3,
                   commandAlteration4
                }
            };

            var order = new Order(Guid.NewGuid(), Description);
            var alteration1 = new Alteration(Guid.NewGuid(), AlterationType.Sleeve, AlterationSide.Left, 1);
            var alteration2 = new Alteration(Guid.NewGuid(), AlterationType.Sleeve, AlterationSide.Right, 2);
            var alteration3 = new Alteration(Guid.NewGuid(), AlterationType.Trouser, AlterationSide.Left, 3);
            var alteration4 = new Alteration(Guid.NewGuid(), AlterationType.Trouser, AlterationSide.Right, 4);

            var commandResult = new CreateOrderCommandResult
            {
                Description = Description,
                Alterations = new List<CreateOrderCommandAlteration>
                {
                   commandAlteration1,
                   commandAlteration2,
                   commandAlteration3,
                   commandAlteration4
                }
            };

            A
                .CallTo(() => _orderFactory.Create(Description))
                .Returns(order);

            A
                .CallTo(() => _alterationFactory.Create(AlterationType.Sleeve, AlterationSide.Left, 1))
                .Returns(alteration1);

            A
                .CallTo(() => _alterationFactory.Create(AlterationType.Sleeve, AlterationSide.Right, 2))
                .Returns(alteration2);

            A
                .CallTo(() => _alterationFactory.Create(AlterationType.Trouser, AlterationSide.Left, 3))
                .Returns(alteration3);

            A
                .CallTo(() => _alterationFactory.Create(AlterationType.Trouser, AlterationSide.Right, 4))
                .Returns(alteration4);

            A
                .CallTo(() => _mapper.Map<Order, CreateOrderCommandResult>(order))
                .Returns(commandResult);

            // Act
            var actualCommandResult = await _handler.Handle(command, CancellationToken.None);

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(actualCommandResult.Description, Is.EqualTo(Description));
                Assert.That(actualCommandResult.Alterations, Contains.Item(commandAlteration1));
                Assert.That(actualCommandResult.Alterations, Contains.Item(commandAlteration2));
                Assert.That(actualCommandResult.Alterations, Contains.Item(commandAlteration3));
                Assert.That(actualCommandResult.Alterations, Contains.Item(commandAlteration4));

                A
                    .CallTo(() => _alterationFactory.Create(AlterationType.Sleeve, AlterationSide.Left, 1))
                    .MustHaveHappened();

                A
                    .CallTo(() => _alterationFactory.Create(AlterationType.Sleeve, AlterationSide.Right, 2))
                    .MustHaveHappened();

                A
                    .CallTo(() => _alterationFactory.Create(AlterationType.Trouser, AlterationSide.Left, 3))
                    .MustHaveHappened();

                A
                    .CallTo(() => _alterationFactory.Create(AlterationType.Trouser, AlterationSide.Right, 4))
                    .MustHaveHappened();

                A
                    .CallTo(() => _orderFactory.Create(Description))
                    .MustHaveHappened();

                A
                    .CallTo(() => _orderRepository.CreateOrder(order))
                    .MustHaveHappened();

                A
                   .CallTo(() => _unitOfWork.Commit())
                   .MustHaveHappened();
            });
        }
    }
}
