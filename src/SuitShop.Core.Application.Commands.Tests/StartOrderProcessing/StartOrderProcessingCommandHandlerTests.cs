﻿using FakeItEasy;
using NUnit.Framework;
using SuitShop.Core.Application.Commands.StartOrderProcessing;
using SuitShop.Core.Domain.Common;
using SuitShop.Core.Domain.Orders;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SuitShop.Core.Application.Commands.Tests.StartOrderProcessing
{
    [TestFixture]
    public sealed class StartOrderProcessingCommandHandlerTests
    {
        private const string Description = "TheDescription";

        private IOrderRepository _orderRepository;
        private IUnitOfWork _unitOfWork;

        private StartOrderProcessingCommandHandler _handler;

        [SetUp]
        public void Setup()
        {
            _orderRepository = A.Fake<IOrderRepository>();
            _unitOfWork = A.Fake<IUnitOfWork>();

            _handler = new StartOrderProcessingCommandHandler(
                _orderRepository,
                _unitOfWork);
        }

        [Test]
        public async Task Handle_TestFlow()
        {
            // Arrange
            var orderId = Guid.NewGuid();
            var command = new StartOrderProcessingCommand
            {
                OrderId = orderId
            };

            var order = new Order(orderId, Description);
            order.Pay();

            A
                .CallTo(() => _orderRepository.GetOrder(orderId))
                .Returns(order);

            // Act
            await _handler.Handle(command, CancellationToken.None);

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(order.Status, Is.EqualTo(OrderStatus.InProcess));

                A
                    .CallTo(() => _orderRepository.GetOrder(orderId))
                    .MustHaveHappened();

                A
                  .CallTo(() => _orderRepository.UpdateOrder(order))
                  .MustHaveHappened();

                A
                   .CallTo(() => _unitOfWork.Commit())
                   .MustHaveHappened();
            });
        }
    }
}
