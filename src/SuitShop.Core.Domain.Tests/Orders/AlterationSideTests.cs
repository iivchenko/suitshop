﻿using NUnit.Framework;
using SuitShop.Core.Domain.Common;
using SuitShop.Core.Domain.Orders;

namespace SuitShop.Core.Domain.Tests.Orders
{
    [TestFixture]
    public sealed class AlterationSideTests
    {
        [TestCase(1, "Left")]
        [TestCase(2, "Right")]
        public void GetById_SideExists_Success(int id, string name)
        {
            var type = AlterationSide.GetById(id);

            Assert.That(type.Id, Is.EqualTo(id));
            Assert.That(type.Name, Is.EqualTo(name));
        }

        [TestCase(-1)]
        [TestCase(0)]
        [TestCase(3)]
        public void GetById_SideNotExists_Throws(int id)
        {
            Assert
                .That(() => AlterationSide.GetById(id),
                Throws.InstanceOf<DomainException>().With.Message.Match($"Undefined Alteration Side Id '{id}'!"));
        }
    }
}
