﻿using NUnit.Framework;
using SuitShop.Core.Domain.Common;
using SuitShop.Core.Domain.Orders;

namespace SuitShop.Core.Domain.Tests.Orders
{
    [TestFixture]
    public sealed class AlterationTypeTests
    {
        [TestCase(1, "Sleeve")]
        [TestCase(2, "Trouser")]
        public void GetById_TypeExists_Success(int id, string name)
        {
            var type = AlterationType.GetById(id);

            Assert.That(type.Id, Is.EqualTo(id));
            Assert.That(type.Name, Is.EqualTo(name));
        }

        [TestCase(-1)]
        [TestCase(0)]
        [TestCase(3)]
        public void GetById_TypeNotExists_Throws(int id)
        {
            Assert
                .That(() => AlterationType.GetById(id),
                Throws.InstanceOf<DomainException>().With.Message.Match($"Undefined Alteration Type Id '{id}'!"));
        }
    }
}
