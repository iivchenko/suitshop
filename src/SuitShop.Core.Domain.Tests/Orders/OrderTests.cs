﻿using NUnit.Framework;
using SuitShop.Core.Domain.Common;
using SuitShop.Core.Domain.Orders;
using System;

namespace SuitShop.Core.Domain.Tests.Orders
{
    [TestFixture]
    public sealed class OrderTests
    {
        private const string Description = "TheDescription";

        [Test]
        public void NewOrder_MissingId_Throws()
        {
            Assert
                .That(
                () => CreateOrder(Guid.Empty, Description),
                Throws.InstanceOf<ArgumentException>().With.Message.Matches("The ID cannot be the type's default value."));
        }

        [Test]
        public void NewOrder_MissingDescription_Throws()
        {
            Assert
                .That(
                () => CreateOrder(Guid.NewGuid(), null),
                Throws.InstanceOf<ArgumentNullException>().With.Message.Matches("Value cannot be null.\r\nParameter name: description"));
        }

        [Test]
        public void NewOrder_AllConditionsAreMet_Success()
        {
            // Arrange 
            var id = Guid.NewGuid();
           
            // Act
            var order = CreateOrder(id, Description);

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(order.Id, Is.EqualTo(id));
                Assert.That(order.Description, Is.EqualTo(Description));
                Assert.That(order.Status, Is.EqualTo(OrderStatus.Created));
                Assert.That(order.Alterations, Is.Empty);
            });
        }

        [Test]
        public void AddAlteration_AllConditionsAreMet_Success()
        {
            // Arrange 
            var id = Guid.NewGuid();
            var order = CreateOrder(id, Description);
            var alteration = new Alteration(Guid.NewGuid(), AlterationType.Sleeve, AlterationSide.Left, 3);

            // Act
            order.AddAlterations(alteration);

            // Assert
            Assert.That(order.Alterations, Contains.Item(alteration));
        }

        [Test]
        public void AddAlteration_NullAlteration_Throws()
        {
            // Arrange 
            var id = Guid.NewGuid();
            var order = CreateOrder(id, Description);
            

            // Act + Assert
            Assert
                .That(
                () => order.AddAlterations(null),
                Throws.InstanceOf<ArgumentNullException>().With.Message.Match("Value cannot be null.\r\nParameter name: alteration"));
        }

        [Test]
        public void AddAlteration_AlterationAllreadyExist_Throws()
        {
            // Arrange 
            var id = Guid.NewGuid();
            var order = CreateOrder(id, Description);
            var alteration = new Alteration(Guid.NewGuid(), AlterationType.Sleeve, AlterationSide.Left, 3);

            order.AddAlterations(alteration);

            // Act + Assert
            Assert
                .That(
                () => order.AddAlterations(alteration),
                Throws.InstanceOf<DomainException>().With.Message.Match($"Aleration with Type {alteration.Type.ToString()} and Side {alteration.Side.ToString()} allready exists!"));
        }

        [Test]
        public void Pay_OrderInCreatedStatus_Success()
        {
            // Arrange 
            var id = Guid.NewGuid();
            var order = CreateOrder(id, Description);

            // Act
            order.Pay();

            // Assert
            Assert.That(order.Status, Is.EqualTo(OrderStatus.Paid));
        }

        [Test]
        public void Pay_OrderInPaidStatus_Throws()
        {
            // Arrange 
            var id = Guid.NewGuid();
            var order = CreateOrder(id, Description);

            order.Pay();

            // Act + Assert
            Assert
                .That(
                () => order.Pay(),
                Throws.InstanceOf<DomainException>().With.Message.Match(CreateStatusErrorMessage(OrderStatus.Paid.Name, OrderStatus.Paid.Name)));
        }

        [Test]
        public void Pay_OrderInInProcessStatus_Throws()
        {
            // Arrange 
            var id = Guid.NewGuid();
            var order = CreateOrder(id, Description);

            order.Pay();
            order.TakeToWork();

            // Act + Assert
            Assert
                .That(
                () => order.Pay(),
                Throws.InstanceOf<DomainException>().With.Message.Match(CreateStatusErrorMessage(OrderStatus.InProcess.Name, OrderStatus.Paid.Name)));
        }

        [Test]
        public void Pay_OrderInDoneStatus_Throws()
        {
            // Arrange 
            var id = Guid.NewGuid();
            var order = CreateOrder(id, Description);

            order.Pay();
            order.TakeToWork();
            order.Finish();

            // Act + Assert
            Assert
                .That(
                () => order.Pay(),
                Throws.InstanceOf<DomainException>().With.Message.Match(CreateStatusErrorMessage(OrderStatus.Done.Name, OrderStatus.Paid.Name)));
        }

        [Test]
        public void TakeToWork_OrderInPaidStatus_Success()
        {
            // Arrange 
            var id = Guid.NewGuid();
            var order = CreateOrder(id, Description);

            order.Pay();
            
            // Act
            order.TakeToWork();

            // Assert
            Assert.That(order.Status, Is.EqualTo(OrderStatus.InProcess));
        }

        [Test]
        public void TakeToWork_OrderInCreatedStatus_Throws()
        {
            // Arrange 
            var id = Guid.NewGuid();
            var order = CreateOrder(id, Description);

            // Act + Assert
            Assert
                .That(
                () => order.TakeToWork(),
                Throws.InstanceOf<DomainException>().With.Message.Match(CreateStatusErrorMessage(OrderStatus.Created.Name, OrderStatus.InProcess.Name)));
        }

        [Test]
        public void TakeToWork_OrderInInProcessStatus_Throws()
        {
            // Arrange 
            var id = Guid.NewGuid();
            var order = CreateOrder(id, Description);

            order.Pay();
            order.TakeToWork();

            // Act + Assert
            Assert
                .That(
                () => order.TakeToWork(),
                Throws.InstanceOf<DomainException>().With.Message.Match(CreateStatusErrorMessage(OrderStatus.InProcess.Name, OrderStatus.InProcess.Name)));
        }

        [Test]
        public void TakeToWork_OrderInDoneStatus_Throws()
        {
            // Arrange 
            var id = Guid.NewGuid();
            var order = CreateOrder(id, Description);

            order.Pay();
            order.TakeToWork();
            order.Finish();

            // Act + Assert
            Assert
                .That(
                () => order.TakeToWork(),
                Throws.InstanceOf<DomainException>().With.Message.Match(CreateStatusErrorMessage(OrderStatus.Done.Name, OrderStatus.InProcess.Name)));
        }

        [Test]
        public void Finish_OrderInInProcessStatus_Success()
        {
            // Arrange 
            var id = Guid.NewGuid();
            var order = CreateOrder(id, Description);

            order.Pay();
            order.TakeToWork();

            // Act
            order.Finish();

            // Assert
            Assert.That(order.Status, Is.EqualTo(OrderStatus.Done));
        }

        [Test]
        public void Finish_OrderInCreatedStatus_Throws()
        {
            // Arrange 
            var id = Guid.NewGuid();
            var order = CreateOrder(id, Description);

            // Act + Assert
            Assert
                .That(
                () => order.Finish(),
                Throws.InstanceOf<DomainException>().With.Message.Match(CreateStatusErrorMessage(OrderStatus.Created.Name, OrderStatus.Done.Name)));
        }

        [Test]
        public void Finish_OrderInPaidStatus_Throws()
        {
            // Arrange 
            var id = Guid.NewGuid();
            var order = CreateOrder(id, Description);

            order.Pay();

            // Act + Assert
            Assert
                .That(
                () => order.Finish(),
                Throws.InstanceOf<DomainException>().With.Message.Match(CreateStatusErrorMessage(OrderStatus.Paid.Name, OrderStatus.Done.Name)));
        }

        [Test]
        public void Finish_OrderInDoneStatus_Throws()
        {
            // Arrange 
            var id = Guid.NewGuid();
            var order = CreateOrder(id, Description);

            order.Pay();
            order.TakeToWork();
            order.Finish();

            // Act + Assert
            Assert
                .That(
                () => order.Finish(),
                Throws.InstanceOf<DomainException>().With.Message.Match(CreateStatusErrorMessage(OrderStatus.Done.Name, OrderStatus.Done.Name)));
        }

        private static Order CreateOrder()
        {
            return CreateOrder(Guid.NewGuid(), Description);
        }

        private static Order CreateOrder(Guid id, string description)
        {
            return new Order(id, description);
        }

        private static string CreateStatusErrorMessage(string initial, string resulting)
        {
            return $"Invalid status update! Can't move order from '{initial}' to '{resulting}' status.";
        }
    }
}
