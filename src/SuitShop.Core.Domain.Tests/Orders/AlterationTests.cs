﻿using NUnit.Framework;
using SuitShop.Core.Domain.Orders;
using System;
using System.Collections.Generic;

namespace SuitShop.Core.Domain.Tests.Orders
{
    [TestFixture]
    public sealed class AlterationTests
    {
        [Test]
        public void NewAlteration_MissingId_Throws()
        {
            Assert
                .That(
                () => CreateAlteration(Guid.Empty, 5, AlterationType.Sleeve, AlterationSide.Left),
                Throws.InstanceOf<ArgumentException>().With.Message.Matches("The ID cannot be the type's default value."));
        }

        [TestCase(-1)]
        [TestCase(0)]
        [TestCase(6)]
        public void NewAlteration_InvalidAmount_Throws(int amount)
        {
            Assert
                .That(() => CreateAlteration(amount),
                Throws.InstanceOf<ArgumentOutOfRangeException>().With.Message.Match($"Invalid alteration amount '{amount}'! Amount should be in range {Alteration.MinAmount} to {Alteration.MaxAmount}"));
        }

        [TestCaseSource(nameof(GetValidAltereationData))]
        public void NewAlteration_ValidAmount_Throws(AlterationData alterationData)
        {
            // Arrange + Act
            var alteration = CreateAlteration(alterationData.Amount, alterationData.Type, alterationData.Side);

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(alteration.Amount, Is.EqualTo(alterationData.Amount));
                Assert.That(alteration.Type, Is.EqualTo(alterationData.Type));
                Assert.That(alteration.Side, Is.EqualTo(alterationData.Side));
            });
        }

        private static IEnumerable<AlterationData> GetValidAltereationData()
        {
            for(var i = Alteration.MinAmount; i <= Alteration.MaxAmount; i++)
            {
                yield return new AlterationData
                {
                    Type = AlterationType.Sleeve,
                    Side = AlterationSide.Left,
                    Amount = i
                };

                yield return new AlterationData
                {
                    Type = AlterationType.Sleeve,
                    Side = AlterationSide.Right,
                    Amount = i
                };

                yield return new AlterationData
                {
                    Type = AlterationType.Trouser,
                    Side = AlterationSide.Left,
                    Amount = i
                };

                yield return new AlterationData
                {
                    Type = AlterationType.Trouser,
                    Side = AlterationSide.Right,
                    Amount = i
                };
            }
        }

        private static Alteration CreateAlteration(int amount)
        {
            return CreateAlteration(Guid.NewGuid(), amount, AlterationType.Sleeve, AlterationSide.Left);
        }

        private static Alteration CreateAlteration(int amount, AlterationType type, AlterationSide side)
        {
            return CreateAlteration(Guid.NewGuid(), amount, type, side);
        }

        private static Alteration CreateAlteration(Guid id, int amount, AlterationType type, AlterationSide side)
        {
            return new Alteration(id, type, side, amount);
        }

        public sealed class AlterationData
        {
            public AlterationType Type { get; set; }

            public AlterationSide Side { get; set; }

            public int Amount { get; set; }
        }
    }
}
