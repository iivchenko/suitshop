﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using SuitShop.Core.Application.Queries.GetOrders;
using SuitShop.Core.Application.Queries.Sql.Common;

namespace SuitShop.Core.Application.Queries.Sql
{
    public sealed class GetOrdersQueryHandler : IGetOrdersQueryHandler
    {
        private readonly IConnectionFactory _connectionFactory;

        public GetOrdersQueryHandler(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public async Task<IEnumerable<GetOrdersQueryResult>> Handle(GetOrdersQuery request, CancellationToken cancellationToken)
        {
            var query = CreateQuery(request.Filter);

            using (var connection = _connectionFactory.Create())
            {
                var orders = new Dictionary<Guid, GetOrdersQueryResult>();

                var theOrders = await connection.QueryAsync<GetOrdersQueryResult, GetOrdersQueryAlteration, GetOrdersQueryResult>(
                    query,
                    (order, alteration) =>
                    {
                        if (!orders.TryGetValue(order.Id, out var theOrder))
                        {
                            theOrder = order;
                            orders.Add(theOrder.Id, theOrder);
                        }

                        theOrder.Alterations.Add(alteration);
                        return theOrder;
                    },
                    GetParameter(request.Filter));

                return theOrders.Distinct();
            }
        }

        private static string CreateQuery(GetOrdersQueryFilter filter)
        {
            return filter == 0 
                ? "SELECT orders.Id, orders.Description, orders.StatusId as Status, alterations.Id, alterations.Amount, alterations.TypeId as Type, alterations.SideId as Side, alterations.OrderId FROM[dbo].[orders] as orders INNER JOIN[dbo].[alterations] as alterations ON alterations.OrderId = orders.Id"
                : "SELECT orders.Id, orders.Description, orders.StatusId as Status, alterations.Id, alterations.Amount, alterations.TypeId as Type, alterations.SideId as Side, alterations.OrderId FROM[dbo].[orders] as orders INNER JOIN[dbo].[alterations] as alterations ON alterations.OrderId = orders.Id WHERE orders.StatusId = @StatusId";
        }

        public static object GetParameter(GetOrdersQueryFilter filter)
        {
            return filter == 0
                ? null
                : new { StatusId = filter };
        }
    }
}
