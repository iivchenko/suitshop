﻿using System.Data.SqlClient;

namespace SuitShop.Core.Application.Queries.Sql.Common
{
    public interface IConnectionFactory
    {
        SqlConnection Create();
    }
}
