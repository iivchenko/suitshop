﻿namespace SuitShop.Core.Application.Queries.GetOrders
{
    public enum GetOrdersQueryAlterationSide
    {
        Left = 1,
        Right = 2
    }
}
