﻿namespace SuitShop.Core.Application.Queries.GetOrders
{
    public sealed class GetOrdersQueryAlteration
    {
        public GetOrdersQueryAlterationType Type { get; private set; }

        public GetOrdersQueryAlterationSide Side { get; private set; }

        public int Amount { get; private set; }
    }
}
