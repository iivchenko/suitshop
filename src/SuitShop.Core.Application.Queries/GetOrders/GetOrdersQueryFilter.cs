﻿using System;

namespace SuitShop.Core.Application.Queries.GetOrders
{
    [Flags]
    public enum GetOrdersQueryFilter
    {
        All = 0,
        Created = 1,
        Paid = 2,
        InProcess = 3,
        Done = 4
    }
}
