﻿using MediatR;
using System.Collections.Generic;

namespace SuitShop.Core.Application.Queries.GetOrders
{
    public sealed class GetOrdersQuery : IRequest<IEnumerable<GetOrdersQueryResult>>
    {
        public GetOrdersQueryFilter Filter { get; set; }
    }
}
