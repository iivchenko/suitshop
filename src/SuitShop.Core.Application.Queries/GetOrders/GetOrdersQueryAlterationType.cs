﻿namespace SuitShop.Core.Application.Queries.GetOrders
{
    public enum GetOrdersQueryAlterationType
    {
        Sleeve = 1,
        Trouser = 2
    }
}
