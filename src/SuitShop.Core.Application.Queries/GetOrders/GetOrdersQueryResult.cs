﻿using System;
using System.Collections.ObjectModel;

namespace SuitShop.Core.Application.Queries.GetOrders
{
    public sealed class GetOrdersQueryResult
    {
        public GetOrdersQueryResult()
        {
            Alterations = new Collection<GetOrdersQueryAlteration>();
        }

        public Guid Id { get; set; }

        public string Description { get; set; }

        public GetOrdersQueryOrderStatus Status { get; set; }     

        public Collection<GetOrdersQueryAlteration> Alterations { get; set; }
    }
}
