﻿using MediatR;
using System.Collections.Generic;

namespace SuitShop.Core.Application.Queries.GetOrders
{
    public interface IGetOrdersQueryHandler : IRequestHandler<GetOrdersQuery, IEnumerable<GetOrdersQueryResult>>
    {
    }
}
