﻿namespace SuitShop.Core.Application.Queries.GetOrders
{
    public enum GetOrdersQueryOrderStatus
    {
       Created = 1,
       Paid = 2,
       InProcess = 3, 
       Done = 4
    }
}
