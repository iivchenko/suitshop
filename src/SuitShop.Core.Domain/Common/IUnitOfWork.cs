﻿using System.Threading.Tasks;

namespace SuitShop.Core.Domain.Common
{
    public interface IUnitOfWork
    {
        Task Commit();
    }
}
