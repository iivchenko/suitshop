﻿using SuitShop.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SuitShop.Core.Domain.Orders
{
    public sealed class Order : Entity<Guid>, IAggregateRoot
    {
        private readonly List<Alteration> _alterations;

        public Order(Guid id, string description)
            : base(id)
        {
            Description = description ?? throw new ArgumentNullException(nameof(description));
            Status = OrderStatus.Created;

            _alterations = new List<Alteration>();
        }

        private Order()
            : base (Guid.NewGuid())
        {
            _alterations = new List<Alteration>();
        }

        public string Description { get; private set; }

        public OrderStatus Status { get; private set; }

        public IEnumerable<Alteration> Alterations => _alterations.AsReadOnly();

        public void AddAlterations(Alteration alteration)
        {
            if (alteration == null)
            {
                throw new ArgumentNullException(nameof(alteration));
            }

            if (_alterations.Any(x => x.Type == alteration.Type && x.Side == alteration.Side))
            {
                throw new DomainException($"Aleration with Type {alteration.Type.ToString()} and Side {alteration.Side.ToString()} allready exists!");
            }

            _alterations.Add(alteration);
        }

        public void Pay()
        {
            UpdateStatus(OrderStatus.Created, OrderStatus.Paid);
        }

        public void TakeToWork()
        {
            UpdateStatus(OrderStatus.Paid, OrderStatus.InProcess);
        }

        public void Finish()
        {
            UpdateStatus(OrderStatus.InProcess, OrderStatus.Done);
        }

        private void UpdateStatus(OrderStatus predicateStatus, OrderStatus nextStatus)
        {
            Status =
                Status.Equals(predicateStatus)
                ? nextStatus
                : throw new DomainException($"Invalid status update! Can't move order from '{Status.Name}' to '{nextStatus.Name}' status.");
        }
    }
}
