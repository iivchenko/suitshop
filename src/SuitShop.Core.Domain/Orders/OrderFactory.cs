﻿using System;

namespace SuitShop.Core.Domain.Orders
{
    public sealed class OrderFactory : IOrderFactory
    {
        public Order Create(string description)
        {
            return new Order(Guid.NewGuid(), description);
        }
    }
}
