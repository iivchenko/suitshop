﻿using SuitShop.Core.Domain.Common;

namespace SuitShop.Core.Domain.Orders
{
    public sealed class AlterationSide : Enumeration
    {
        public static AlterationSide Left = new AlterationSide(1, nameof(Left));
        public static AlterationSide Right = new AlterationSide(2, nameof(Right));

        public AlterationSide(int id, string name) 
            : base(id, name)
        {
        }

        public static AlterationSide GetById(int id)
        {
            switch (id)
            {
                case 1:
                    return Left;

                case 2:
                    return Right;

                default:
                    throw new DomainException($"Undefined Alteration Side Id '{id}'!");
            }
        }
    }
}
