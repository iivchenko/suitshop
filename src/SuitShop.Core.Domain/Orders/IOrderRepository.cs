﻿using SuitShop.Core.Domain.Common;
using System;
using System.Threading.Tasks;

namespace SuitShop.Core.Domain.Orders
{
    public interface IOrderRepository : IRepository<Order, Guid>
    {
        Task<Order> GetOrder(Guid id);

        Task CreateOrder(Order order);

        Task UpdateOrder(Order order);
    }
}
