﻿namespace SuitShop.Core.Domain.Orders
{
    public interface IAlteratioinFactory
    {
        Alteration Create(AlterationType type, AlterationSide side, int amount);
    }
}
