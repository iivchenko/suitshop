﻿using System;

namespace SuitShop.Core.Domain.Orders
{
    public sealed class AlteratioinFactory : IAlteratioinFactory
    {
        public Alteration Create(AlterationType type, AlterationSide side, int amount)
        {
            return new Alteration(Guid.NewGuid(), type, side, amount);
        }
    }
}
