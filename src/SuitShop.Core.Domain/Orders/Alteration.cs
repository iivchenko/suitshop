﻿using SuitShop.Core.Domain.Common;
using System;

namespace SuitShop.Core.Domain.Orders
{
    public sealed class Alteration : Entity<Guid>
    {
        public static readonly int MinAmount = 1;
        public static readonly int MaxAmount = 5;

        public Alteration(Guid id, AlterationType type, AlterationSide side, int amount)
            : base(id)
        {
            if (MaxAmount < amount || amount < MinAmount)
            {
                throw new ArgumentOutOfRangeException($"Invalid alteration amount '{amount}'! Amount should be in range {MinAmount} to {MaxAmount}");
            }

            Type = type;
            Side = side;
            Amount = amount;
        }

        private Alteration()
            : base(Guid.NewGuid())
        {
        }

        public AlterationType Type { get; private set; }

        public AlterationSide Side { get; private set; }

        public int Amount { get; private set; }
    }
}
