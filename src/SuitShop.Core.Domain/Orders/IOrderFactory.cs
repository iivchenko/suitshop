﻿namespace SuitShop.Core.Domain.Orders
{
    public interface IOrderFactory
    {
        Order Create(string description);
    }
}
