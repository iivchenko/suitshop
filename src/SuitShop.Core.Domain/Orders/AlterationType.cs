﻿using SuitShop.Core.Domain.Common;

namespace SuitShop.Core.Domain.Orders
{
    public sealed class AlterationType : Enumeration
    {
        public static AlterationType Sleeve = new AlterationType(1, nameof(Sleeve));
        public static AlterationType Trouser = new AlterationType(2, nameof(Trouser));

        public AlterationType(int id, string name) 
            : base(id, name)
        {
        }

        public static AlterationType GetById(int id)
        {
            switch (id)
            {
                case 1:
                    return Sleeve;

                case 2:
                    return Trouser;

                default:
                    throw new DomainException($"Undefined Alteration Type Id '{id}'!");
            }
        }
    }
}
