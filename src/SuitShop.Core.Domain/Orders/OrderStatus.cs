﻿using SuitShop.Core.Domain.Common;

namespace SuitShop.Core.Domain.Orders
{
    public sealed class OrderStatus : Enumeration
    {
        public static OrderStatus Created = new OrderStatus(1, nameof(Created));
        public static OrderStatus Paid = new OrderStatus(2, nameof(Paid));
        public static OrderStatus InProcess = new OrderStatus(3, nameof(InProcess));
        public static OrderStatus Done = new OrderStatus(4, nameof(Done));

        public OrderStatus(int id, string name) 
            : base(id, name)
        {
        }
    }
}
