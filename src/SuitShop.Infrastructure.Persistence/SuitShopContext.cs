﻿using Microsoft.EntityFrameworkCore;
using SuitShop.Core.Domain.Orders;
using SuitShop.Infrastructure.Persistence.Orders;

namespace SuitShop.Infrastructure.Persistence
{
    public sealed class SuitShopContext : DbContext
    {
        public SuitShopContext(DbContextOptions<SuitShopContext> options)
            : base(options)
        {
        }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderStatus> OrderStatuses { get; set; }

        public DbSet<Alteration> Alterations { get; set; }

        public DbSet<AlterationType> AlterationTypes { get; set; }

        public DbSet<AlterationSide> AlterationSides { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new AlterationTypeMapping());
            builder.ApplyConfiguration(new AlterationSideMapping());
            builder.ApplyConfiguration(new AlterationMapping());
            builder.ApplyConfiguration(new OrderStatusMapping());
            builder.ApplyConfiguration(new OrderMapping());
        }
    }
}
