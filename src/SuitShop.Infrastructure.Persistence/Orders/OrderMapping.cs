﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SuitShop.Core.Domain.Orders;

namespace SuitShop.Infrastructure.Persistence.Orders
{
    public sealed class OrderMapping : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder
               .ToTable("orders");

            builder
                .HasKey(x => x.Id);

            builder
                .Property(x => x.Description)
                .HasMaxLength(50);

            builder
               .HasOne(x => x.Status);

            builder
                .HasMany(x => x.Alterations);
        }
    }
}
