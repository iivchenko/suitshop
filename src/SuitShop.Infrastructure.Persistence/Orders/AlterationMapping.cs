﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SuitShop.Core.Domain.Orders;

namespace SuitShop.Infrastructure.Persistence.Orders
{
    public sealed class AlterationMapping : IEntityTypeConfiguration<Alteration>
    {
        public void Configure(EntityTypeBuilder<Alteration> builder)
        {
            builder
               .ToTable("alterations");

            builder
                .HasKey(x => x.Id);

            builder
                .Property(x => x.Amount);

            builder
                .HasOne(x => x.Side);

            builder
                .HasOne(x => x.Type);
        }
    }
}
