﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SuitShop.Core.Domain.Orders;

namespace SuitShop.Infrastructure.Persistence.Orders
{
    public sealed class AlterationTypeMapping : IEntityTypeConfiguration<AlterationType>
    {
        public void Configure(EntityTypeBuilder<AlterationType> builder)
        {
            builder
               .ToTable("alterationTypes");

            builder
                .HasKey(o => o.Id);

            builder
                .Property(o => o.Id)
                .HasDefaultValue(1)
                .ValueGeneratedNever()
                .IsRequired();

            builder
                .Property(o => o.Name)
                .HasMaxLength(10)
                .IsRequired();
        }
    }
}
