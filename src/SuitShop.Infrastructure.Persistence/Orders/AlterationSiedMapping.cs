﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SuitShop.Core.Domain.Orders;

namespace SuitShop.Infrastructure.Persistence.Orders
{
    public sealed class AlterationSideMapping : IEntityTypeConfiguration<AlterationSide>
    {
        public void Configure(EntityTypeBuilder<AlterationSide> builder)
        {
            builder
               .ToTable("alterationSides");

            builder
                .HasKey(o => o.Id);

            builder
                .Property(o => o.Id)
                .HasDefaultValue(1)
                .ValueGeneratedNever()
                .IsRequired();

            builder
                .Property(o => o.Name)
                .HasMaxLength(10)
                .IsRequired();
        }
    }
}
