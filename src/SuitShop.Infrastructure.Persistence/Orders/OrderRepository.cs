﻿using System;
using System.Threading.Tasks;
using SuitShop.Core.Domain.Orders;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace SuitShop.Infrastructure.Persistence.Orders
{
    public sealed class OrderRepository : IOrderRepository
    {
        private SuitShopContext _context;

        public OrderRepository(SuitShopContext context)
        {
            _context = context;
        }

        public async Task CreateOrder(Order order)
        {
            // TODO: Looks ugly, logic spread accorss the app. Need to fix this ugly bug
            foreach(var side in order.Alterations.Select(x => x.Side))
            {
                _context.Attach(side);
            }

            foreach (var type in order.Alterations.Select(x => x.Type))
            {
                _context.Attach(type);
            }

            _context.Attach(order.Status);

            await _context.Orders.AddAsync(order);
        }

        public async Task<Order> GetOrder(Guid id)
        {
            return
                await
                _context
                    .Orders
                    .Include(x => x.Status)
                    .SingleAsync(x => x.Id == id);
        }

        public Task UpdateOrder(Order order)
        {
            _context.Entry(order).State = EntityState.Modified;

            // TODO: Looks ugly, logic spread accorss the app. Need to fix this ugly bug
            _context.Entry(order.Status).State = EntityState.Unchanged;

            return Task.CompletedTask;
        }
    }
}
