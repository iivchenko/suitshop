﻿using SuitShop.Core.Domain.Common;
using System.Threading.Tasks;

namespace SuitShop.Infrastructure.Persistence
{
    public sealed class UnitOfWork : IUnitOfWork
    {
        private readonly SuitShopContext _context;

        public UnitOfWork(SuitShopContext context)
        {
            _context = context;
        }

        public async Task Commit()
        {
            await _context.SaveChangesAsync();
        }
    }
}