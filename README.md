# SuitShop

SuitShop is an implementation of .NET Software Engineer Technical Assignment task.

# How to use

## Prerequisites
* docker 
* docker-compose
* .NET Core SDK 2.2.107
* PowerShell

## Before runing scripts
* Run cmd 
* Navigate to the app root folder
* ...

## Build
Next command will build application and create docker images
```
     .\build.ps1 -Target Build
```
## Run 
* Next command will build application, create docker images and run docker-compose up so services start in detached mode.
```
     .\build.ps1 -Target Run
```
* MVC application will open index page immediately after start.
* MVC is accesable with swagger: **http://localhost:8880/**
* Api is accesable with swagger: **http://localhost:7780/swagger**

## Test
Before running tests be sure that services are running. Execute **Run** first.
The test command will build functional tests and will run them.
```
     .\build.ps1 -Target Test
```

# What was used
## Tech Stack:
* ASP.NTET Core
* EF
* Dapper
* MS SQL Server
* Docker
* Docker Compose
* Cake
* Swagger
* Automapper
* MediatR
* FluentValidation
* PowerShell
* RabbitMq
* Azure Bus
* Azure Insights

## Principles:
* SOLID
* REST
* Clean Arhitecture
* CQRS
* DDD
* TDD
* Feature Folders

# License

Suit Shop is open source software, licensed under the terms of MIT license. 
See [LICENSE](LICENSE) for details.