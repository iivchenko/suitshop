#addin Cake.Docker&version=0.10.0
#tool nuget:?package=NUnit.ConsoleRunner&version=3.10.0
var target = Argument("target", "Build");

Task("Build")
    .Does(() =>
{
	DockerComposeBuild("suitshop.api", "suitshop.web");
});

Task("Run")
	.IsDependentOn("Build")
    .Does(() =>
{
	var settings = new DockerComposeUpSettings { DetachedMode = true };
	DockerComposeUp(settings, "suitshop.api", "suitshop.web");
});

Task("Build-Tests")	
    .Does(() =>
{
	DotNetCoreBuild("SuitShop.Tests.sln");
});

Task("Test")
	.IsDependentOn("Build-Tests")
    .Does(() =>
{
	DotNetCoreTest("./tests/SuitShop.Tests.Functional/SuitShop.Tests.Functional.csproj");
});

RunTarget(target);